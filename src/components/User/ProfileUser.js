import React, { Component } from 'react';
import {Alert, Button, ButtonGroup, Container, ListGroup} from 'react-bootstrap';
import {FaEye, FaPen, FaPlus, FaTrashAlt, FaUsers} from "react-icons/fa";
import {connect} from "react-redux";
import PropTypes from "prop-types";
import { getUser } from './userActions';
import {Link, Route, Switch} from "react-router-dom";
import AddUser from "./addUser";

class ProfileUser extends Component {

    componentDidMount() {
        this.props.getUser();
    }

    render() {
        const content = this.props.user.map(u => (
            <ListGroup.Item className="d-flex align-items-center">
                <div className="mr-5">{u.firstName} {u.lastName}</div>
                <div className="mx-5">{u.email}</div>
                <div className="mx-5">06/12/2019</div>
                <ButtonGroup className="ml-auto">
                    <Button className="btn-success"><FaPen /></Button>
                    <Button ><FaEye /></Button>
                    <Button className="btn-danger"><FaTrashAlt /></Button>
                </ButtonGroup>
            </ListGroup.Item>
        ));

        return (
            <Container className="py-3 px-5">
                <Switch>
                    <Route path="/commercial/users/add">
                        <AddUser />
                    </Route>
                    <Route path="/commercial/users">
                        <h2><FaUsers/> Liste des utilisateurs</h2>
                        <Alert variant="dark" className="text-right">
                            <Button as={Link} to="/commercial/users/add" className="btn-success"><FaPlus /></Button>
                        </Alert>
                        <ListGroup variant="flush">
                            { content }
                        </ListGroup>
                    </Route>
                </Switch>
            </Container>
        )
    };
}

ProfileUser.propTypes = {
    user: PropTypes.array.isRequired,
    errors: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
    user: state.user.items,
    errors: state.errors
})

export default connect(mapStateToProps, { getUser })(ProfileUser)
