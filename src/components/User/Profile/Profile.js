import React, { Component } from 'react';
import {Container, ListGroup} from 'react-bootstrap';
import {getCurrentUser} from "../userActions";
import PropTypes from "prop-types";
import {connect} from "react-redux";

class Profile extends Component {

    componentDidMount(props) {
        this.props.getCurrentUser(this.props.user._id)
    }

    render() {
        return (
            <Container className="my-5">
                <h1>User Profile</h1>
                <div>
                    <ListGroup.Item className="d-flex align-items-center">
                        <div className="mr-5">{this.props.userCurrent.firstName} {this.props.userCurrent.lastName}</div>
                        <div className="mx-5">{this.props.userCurrent.email}</div>
                    </ListGroup.Item>
                </div>
            </Container>
        )
    }
}

Profile.propTypes = {
    userCurrent: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired,
}

const mapStateToProps = state => ({
    userCurrent: state.user.items,
    errors: state.errors,
})

export default connect(mapStateToProps, { getCurrentUser })(Profile)
