import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Image, ButtonGroup, Button } from 'react-bootstrap';
import { addToBasket, removeFromBasket } from '../../../actions/basketActions'

const pillPic = [
    "https://cdn.shopify.com/s/files/1/0019/1843/5381/products/Box_Mockups_OK_3_241036b2-175b-484e-960d-3c632a6e0f48_800x.jpg",
    "https://www.decolore.net/wp-content/uploads/2018/08/pills-box-packaging-psd-mockup-templates-cover.png",
    "https://cdn.shopify.com/s/files/1/0019/1843/5381/products/Box_Mockups_OK_2_b6ed3ef7-bf46-4127-b18f-57f96fffe450_800x.jpg",
    "https://image.freepik.com/free-psd/medical-pillbox-mock-up_110893-1582.jpg",
    "https://image.freepik.com/free-psd/pill-bottle-mockup_77847-103.jpg",
    "https://s3.envato.com/files/245086193/07_PillsBox-Mockup2-4.jpg",
    "https://i.pinimg.com/originals/44/4d/c1/444dc1f68ad244809e62af43420b6b5e.jpg"
]

class Basket extends Component {

    addOne = (id, name, list_price) => {
        this.props.addToBasket({
            basketId: this.props.basket._id,
            product: {
                id: id,
                name: name,
                list_price: list_price
            }
        })
    }

    removeOne = (id, name, list_price) => {
        this.props.removeFromBasket({
            basketId: this.props.basket._id,
            product: {
                id: id,
                name: name,
                list_price: list_price
            }
        })
    }

    render () {
        let basket = <tr key="empty"><td colSpan={4}>Votre panier est vide</td></tr>
        
        if (this.props.basket.items.length) {
            basket = this.props.basket.items.map((product) => {
                let total = product.list_price*product.quantity
                return (
                    <tr key={product.id}>
                        <td>
                            <div className="d-flex align-items-center">
                                <Image src={pillPic[Math.floor(Math.random() * (pillPic.length - 1) + 1) - 1]} style={{maxWidth:"90px"}} rounded />
                                <h6 className="ml-4">{product.name}</h6>
                            </div>
                        </td>
                        <td className="align-middle">
                            <div className="d-flex justify-content-center align-items-center">
                                <h4>{product.list_price}€</h4>
                            </div>
                        </td>
                        <td className="align-middle">
                            <div className="d-flex justify-content-center align-items-center">
                                <h4 className="mr-4">{product.quantity}</h4>
                                <ButtonGroup>
                                    <Button variant="success" onClick={() => this.addOne(product.id, product.name, product.list_price)}>+</Button>
                                    <Button variant="danger" onClick={() => this.removeOne(product.id, product.name, product.list_price)}>-</Button>
                                </ButtonGroup>
                            </div>
                        </td>
                        <td className="align-middle">
                            <div className="d-flex justify-content-center align-items-center">
                                <h4>{parseFloat(total.toFixed(2))}€</h4>
                            </div>
                        </td>
                    </tr>
                )
            });
            basket.push(<tr key="total"><td colSpan={4}>Total : {parseFloat(this.props.basket.total_price.toFixed(2))}€</td></tr>)
        }
        return (basket)
    }
}

Basket.propTypes = {
    basket: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
    basket: state.basket.basket
})

export default connect(mapStateToProps, { addToBasket, removeFromBasket })(Basket);
