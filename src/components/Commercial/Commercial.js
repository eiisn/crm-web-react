import React, { Component } from 'react';
import PropTypes from 'prop-types';
import '../../assets/css/Commercial.css'
import { Switch, Route, withRouter } from 'react-router-dom';
import { Col, Container, Row } from 'react-bootstrap';
import Navbar from './Navbar';
import Sidebar from './Sidebar';
import Dashboard from './Dashboard/Dashboard';
import RegisterUser from '../User/Auth/RegisterUser';
import UserList from '../User/ListUser';
import { connect } from 'react-redux';
import ListShipment from "../Shipment/ListShipment";
import ListProduct from "../Product/ListProduct";
import Profile from "../User/Profile/Profile";
import Order from "./Cart/Order";
import Cart from "./Cart/Cart";

class Commercial extends Component {

    componentDidMount(props) {
        if (!this.props.isAuthenticated) {
            this.props.history.push('/login')
        } else {
            if (!this.props.isCommercial) {
                this.props.history.push('/client')
            }
        }
    }
    
    render () {
        return (
            <Container fluid style={{overflow: "hidden"}}>
                <Row>
                    <Sidebar />
                    <Col lg="10" className="p-0">
                        <Navbar />
                        <div className="landing-content overflow-auto">
                            <div className="py-3 px-5">
                                <Switch>
                                    <Route path="/commercial/profile">
                                        <Profile user={this.props.user} />
                                    </Route>
                                    <Route path="/commercial/settings">
                                        <RegisterUser/>
                                    </Route>
                                    <Route path="/commercial/shipments">
                                        <ListShipment />
                                    </Route>
                                    <Route path="/commercial/users">
                                        <UserList />
                                    </Route>
                                    <Route path="/commercial/order">
                                        <Order/>
                                    </Route>
                                    <Route path="/commercial/cart">
                                        <Cart />
                                    </Route>
                                    <Route path="/commercial/profile"></Route>
                                    <Route path="/commercial/list-product">
                                        <ListProduct />
                                    </Route>
                                    <Route exact path={`${this.props.location.pathname}`}>
                                        <Dashboard />
                                    </Route>
                                </Switch>
                            </div>
                        </div>
                    </Col>
                </Row>
            </Container>
        )
    }
}


Commercial.propTypes = {
    isAuthenticated: PropTypes.bool,
    isCommercial: PropTypes.bool,
    user: PropTypes.object
}

const mapStateToProps = state => ({
    isAuthenticated: state.auth.isAuthenticated,
    isCommercial: state.auth.user.isCommercial,
    user: state.auth.user
})


export default withRouter(connect(mapStateToProps)(Commercial));
