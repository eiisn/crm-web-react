import React, { Component } from 'react';
import { Container } from 'react-bootstrap';
import Sale from './Sale';
import Recap from "./Recap";
import VisitedPage from './VisitedPage';


class Dashboard extends Component{
    render() {
        return (
            <Container fluid className="px-4 py-3">
                <Recap/>
                <Sale/>
                <VisitedPage/>
            </Container>
        )
    }
}

export default Dashboard;
