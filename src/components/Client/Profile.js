import React, { Component } from 'react';
import {Container, ListGroup, Form, Card, Col, Alert, Button, ButtonGroup} from 'react-bootstrap';
import {getCurrentUser} from "../User/userActions";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import moment from "moment";
import {Link} from "react-router-dom";
import {FaEye, FaPen, FaTrashAlt} from "react-icons/fa";

class Profile extends Component {

    componentDidMount(props) {
        this.props.getCurrentUser(this.props.user._id)
    }

    render() {
        return (
            <Container className="my-5">
                <Form>
                    <Card.Header as="h5">User Profile</Card.Header>
                    <Card.Body>
                        <Form.Row>
                            <Form.Group as={Col} controlId="email">
                                <Form.Label>Email</Form.Label>
                                <Form.Control
                                    value={this.props.userCurrent.email}
                                    type="text" disabled />
                            </Form.Group>
                        </Form.Row>
                        <Form.Row>
                            <Form.Group as={Col} controlId="lastName">
                                <Form.Label>Prénom</Form.Label>
                                <Form.Control
                                    value={this.props.userCurrent.firstName}
                                    type="text" disabled />
                            </Form.Group>
                            <Form.Group as={Col} controlId="firstName">
                                <Form.Label>Nom</Form.Label>
                                <Form.Control
                                    value={this.props.userCurrent.lastName}
                                    type="text" disabled />
                            </Form.Group>
                        </Form.Row>
                        <Form.Row>
                            <Form.Group as={Col} controlId="phone">
                                <Form.Label>Téléphone</Form.Label>
                                <Form.Control
                                    value={this.props.userCurrent.phone}
                                    type="text" disabled />
                            </Form.Group>
                            <Form.Group as={Col} controlId="enterprise">
                                <Form.Label>Entreprise</Form.Label>
                                <Form.Control
                                    value={this.props.userCurrent.enterprise}
                                    type="text" disabled />
                            </Form.Group>
                        </Form.Row>
                    </Card.Body>
                </Form>
            </Container>
        )
    }
}

Profile.propTypes = {
    userCurrent: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired,
}

const mapStateToProps = state => ({
    userCurrent: state.user.items,
    errors: state.errors,
})

export default connect(mapStateToProps, { getCurrentUser })(Profile)
