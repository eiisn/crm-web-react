import React, { Component } from 'react';
import {Container} from 'react-bootstrap';
import Register from "./Auth/Register";

class AddUser extends Component {

    render() {
        return (
            <Container className="py-3 px-5">
                <Register />
            </Container>
        )
    };
}

export default AddUser;
