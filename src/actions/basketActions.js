import {
    GET_BASKET,
    GET_PREVIOUS_BASKET,
    GET_ERRORS,
    ADD_TO_BASKET,
    REMOVE_FROM_BASKET
} from './types'
import axios from 'axios';


export const getBasket = () => dispatch => {
    axios.get('/api/basket')
    .then(res => {
        dispatch({
            type: GET_BASKET,
            payload: res.data
        })
    })
    .catch(err => {
        dispatch({
            type: GET_ERRORS,
            payload: err
        })
    })
}


export const getPreviousBasket = () => dispatch => {
    axios.get('/api/basket/previous')
    .then(res => {
        dispatch({
            type: GET_PREVIOUS_BASKET,
            payload: res.data
        })
    })
    .catch(err => {
        dispatch({
            type: GET_ERRORS,
            payload: err
        })
    })
}


export const addToBasket = (product) => dispatch => {
    axios.post('/api/basket/add-item', product)
    .then(res => {
        dispatch({
            type: ADD_TO_BASKET,
            payload: res.data
        })
    })
    .catch(err => {
        dispatch({
            type: GET_ERRORS,
            payload: err
        })
    })
}


export const removeFromBasket = (product) => dispatch => {
    axios.post('/api/basket/remove-item', product)
    .then(res => {
        dispatch({
            type: REMOVE_FROM_BASKET,
            payload: res.data
        })
    })
    .catch(err => {
        dispatch({
            type: GET_ERRORS,
            payload: err
        })
    })
}
