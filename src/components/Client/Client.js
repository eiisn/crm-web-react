import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Row, Container } from 'react-bootstrap';
import { Switch, Route, withRouter } from 'react-router-dom';
import Navbar from './Navbar';
import Cart from './Cart/Cart';
import ListProduct from '../Product/ListProduct';
import { getBasket } from "../../actions/basketActions";
import Order from "./Cart/Order";
import Profile from "./Profile";


class Client extends Component {

    componentDidMount() {
        if (!this.props.isAuthenticated) {
            this.props.history.push('/login')
        } else {
            if (this.props.isCommercial) {
                this.props.history.push('/commercial')
            } else {
                this.props.getBasket()
            }
        }
    }

    render() {
        return (
            <>
                <Navbar />
                <Container>
                    <Switch>
                        <Route path="/client/cart">
                            <Cart />
                        </Route>
                        <Route path="/client/profile" >
                            <Profile user={this.props.user} />
                        </Route>
                        <Route exact path="/client/order">
                            <Order/>
                        </Route>
                        <Route exact path={`${this.props.location.pathname}`}>
                            <ListProduct />
                        </Route>
                    </Switch>
                </Container>
            </>
        )
    }
}


Client.propTypes = {
    isAuthenticated: PropTypes.bool,
    isCommercial: PropTypes.bool,
    user: PropTypes.object
}

const mapStateToProps = state => ({
    isAuthenticated: state.auth.isAuthenticated,
    isCommercial: state.auth.user.isCommercial,
    user: state.auth.user
})


export default withRouter(connect(mapStateToProps, { getBasket })(Client));
