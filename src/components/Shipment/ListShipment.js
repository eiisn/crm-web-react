import React, { Component } from 'react';
import {Button, ButtonGroup,ListGroup} from 'react-bootstrap';
import {FaTrashAlt, FaBoxOpen, FaFileDownload} from "react-icons/fa";

class ListShipment extends Component {
    render() {
        return (
            <div className="py-3 px-5">
                <h2><FaBoxOpen/> Liste des commandes</h2>
                <ListGroup variant="flush">
                    <ListGroup.Item className="d-flex align-items-center">
                        <div className="mr-5">#366547</div>
                        <div className="mx-5">Pharmacie République</div>
                        <div className="mx-5">06/12/2019</div>
                        <div className="mx-5">2599,99€</div>
                        <ButtonGroup className="ml-auto">
                            <Button className="btn-success"><FaFileDownload /></Button>
                            <Button className="btn-danger"><FaTrashAlt /></Button>
                        </ButtonGroup>
                    </ListGroup.Item>
                    <ListGroup.Item className="d-flex align-items-center">
                        <div className="mr-5">#366547</div>
                        <div className="mx-5">Pharmacie République</div>
                        <div className="mx-5">06/12/2019</div>
                        <div className="mx-5">2599,99€</div>
                        <ButtonGroup className="ml-auto">
                            <Button className="btn-success"><FaFileDownload /></Button>
                            <Button className="btn-danger"><FaTrashAlt /></Button>
                        </ButtonGroup>
                    </ListGroup.Item>
                    <ListGroup.Item className="d-flex align-items-center">
                        <div className="mr-5">#366547</div>
                        <div className="mx-5">Pharmacie République</div>
                        <div className="mx-5">06/12/2019</div>
                        <div className="mx-5">2599,99€</div>
                        <ButtonGroup className="ml-auto">
                            <Button className="btn-success"><FaFileDownload /></Button>
                            <Button className="btn-danger"><FaTrashAlt /></Button>
                        </ButtonGroup>
                    </ListGroup.Item>
                    <ListGroup.Item className="d-flex align-items-center">
                        <div className="mr-5">#366547</div>
                        <div className="mx-5">Pharmacie République</div>
                        <div className="mx-5">06/12/2019</div>
                        <div className="mx-5">2599,99€</div>
                        <ButtonGroup className="ml-auto">
                            <Button className="btn-success"><FaFileDownload /></Button>
                            <Button className="btn-danger"><FaTrashAlt /></Button>
                        </ButtonGroup>
                    </ListGroup.Item>
                </ListGroup>
            </div>
        )
    };
}

export default ListShipment;
