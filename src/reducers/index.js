import {
    combineReducers
} from "redux";

import authReducer from "./authReducer";
import basketReducer from "./basketReducer";
import errorReducer from "./errorReducer";
import productReducer from "./productReducer";
import shipmentReducer from "./shipmentReducer";
import usersReducer from "./usersReducer";


export default combineReducers({
    auth: authReducer,
    basket: basketReducer,
    errors: errorReducer,
    products: productReducer,
    shipment: shipmentReducer,
    user: usersReducer,
    users: usersReducer,
});
