import {
    GET_USER,
    GET_USERS
} from "../actions/types";

const initialState = {
    items: []
};

export default function (state = initialState, action) {
    switch (action.type) {
        case GET_USERS:
            return {
                ...state,
                items: action.payload
            };
        case GET_USER:
            return {
                ...state,
                items: action.payload
            };
        default:
            return state;
    }
}
