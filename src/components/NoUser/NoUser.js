import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Switch, Route, withRouter } from 'react-router-dom';
import Login from '../User/Auth/Login';
import { Row } from 'react-bootstrap';
import { connect } from 'react-redux';


class Routing extends Component {

    componentDidMount() {
        if (this.props.isAuthenticated) {
            if (this.props.isCommercial) {
                this.props.history.push('/commercial')
            } else {
                this.props.history.push('/client')
            }
        }
    }

    render () {
        return (
            <Row>
                <Switch>
                    <Route path="/">
                        <Login />
                    </Route>
                </Switch>
            </Row>
        )
    }
}


Routing.propTypes = {
    isAuthenticated: PropTypes.bool,
    isCommercial: PropTypes.bool
}


const mapStateToProps = state => ({
    isAuthenticated: state.auth.isAuthenticated,
    isCommercial: state.auth.isCommercial
})


export default withRouter(connect(mapStateToProps)(Routing));
