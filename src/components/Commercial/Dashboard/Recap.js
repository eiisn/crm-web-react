import React, { Component } from 'react';
import { Col, Row, Card } from 'react-bootstrap';
import { FaUsers, FaWallet, FaShoppingCart, FaFile } from 'react-icons/fa';


class Recap extends Component {

    render() {
        return(
            <Row>
                <Col md="3">
                    <Card>
                        <Card.Body>
                            <Row>
                                <Col md="4" className="d-flex align-items-center">
                                    <p className="h1">
                                        <FaShoppingCart className="text-primary"/>
                                    </p>
                                </Col>
                                <Col md="8" className="text-right">
                                    <h6>Stock products</h6>
                                    <h2>1254</h2>
                                </Col>
                            </Row>
                            Go somewhere
                        </Card.Body>
                    </Card>
                </Col>
                <Col md="3">
                    <Card>
                        <Card.Body>
                            <Row>
                                <Col md="4" className="d-flex align-items-center">
                                    <p className="h1">
                                        <FaWallet className="text-success"/>
                                    </p>
                                </Col>
                                <Col md="8" className="text-right">
                                    <h6>Revenue</h6>
                                    <h2>$1.354</h2>
                                </Col>
                            </Row>
                            Go somewhere
                        </Card.Body>
                    </Card>
                </Col>
                <Col md="3">
                    <Card>
                        <Card.Body>
                            <Row>
                                <Col md="4" className="d-flex align-items-center">
                                    <p className="h1">
                                        <FaFile className="text-danger"/>
                                    </p>
                                </Col>
                                <Col md="8" className="text-right">
                                    <h6>Commandes</h6>
                                    <h2>32</h2>
                                </Col>
                            </Row>
                            Go somewhere
                        </Card.Body>
                    </Card>
                </Col>
                <Col md="3">
                    <Card>
                        <Card.Body>
                            <Row>
                                <Col md="4" className="d-flex align-items-center">
                                    <p className="h1">
                                        <FaUsers className="text-info"/>
                                    </p>
                                </Col>
                                <Col md="8" className="text-right">
                                    <h6>Users</h6>
                                    <h2>28</h2>
                                </Col>
                            </Row>
                            Go somewhere
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        )
    }
}

export default Recap;
