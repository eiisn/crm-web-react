import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import { FaUserAstronaut, FaTruck } from 'react-icons/fa';
import { Navbar as BNavbar, Nav, Image } from 'react-bootstrap';
import { logoutUser } from '../../actions/authActions'
import NavCart from './Cart/NavCart';
import Logo from '../../assets/img/logo-bvm.png'


class Navbar extends Component {

    onLogoutClick = e => {
        e.preventDefault();
        this.props.logoutUser();
        window.location.reload();
    };

    render() {
        return (
            <BNavbar bg="dark" variant="dark">
                <BNavbar.Brand as={Link} to="/client" className="mx-auto">
                    <Image src={Logo} thumbnail className="border-0" style={{width:30}} />
                </BNavbar.Brand>
                <BNavbar.Toggle aria-controls="reponsive-navbar-nav"/>
                <BNavbar.Collapse id="responsive-navbar-nav">
                    {this.props.isAuthenticated ?
                        <Nav className="ml-auto">
                            <Nav.Link as={Link} to="/client/profile" className="mx-1"><FaUserAstronaut/></Nav.Link>
                            <NavCart />
                            <Nav.Link as={Link} to="/client/order" className="mx-1"><FaTruck/></Nav.Link>
                            <Nav.Link onClick={this.onLogoutClick}  className="mx-1">Logout</Nav.Link>
                        </Nav> :
                        <Nav className="ml-auto">
                            <Nav.Link as={Link} to="/login">Login</Nav.Link>
                        </Nav>
                    }
                </BNavbar.Collapse>
            </BNavbar>
        )
    }
}

Navbar.propTypes = {
    logoutUser: PropTypes.func.isRequired,
    isAuthenticated: PropTypes.bool.isRequired,
};


const mapStateToProps = state => ({
    isAuthenticated: state.auth.isAuthenticated,
});


export default connect(mapStateToProps, { logoutUser })(withRouter(Navbar));
