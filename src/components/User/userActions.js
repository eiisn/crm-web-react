import axios from "axios";
import jwt_decode from "jwt-decode";
import {GET_USERS, GET_USER} from "../../actions/types";
import setAuthToken from "../../utils/setAuthToken";
export const GET_ERRORS = "GET_ERRORS";

export const listUser = userData => dispatch => {
    axios
        .post("/api/login", userData)
        .then(res => {
            const { token } = res.data;
            localStorage.setItem("token", token);
            setAuthToken(token);
            jwt_decode(token);
            window.location.reload();
        })
        .catch(err =>
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            })
        );
};

// Get product from CRM API NodeJS
export const getUsers = () => dispatch => {
    axios
        .get("/api/users")
        .then(res => {
            dispatch({
                type: GET_USERS,
                payload: res.data
            })
        }).catch(err => {
        dispatch({
            type: GET_ERRORS,
            payload: err
        })
    })
}

// Get product from CRM API NodeJS
export const getCurrentUser = (id) => dispatch => {
    axios
        .get(`/api/user/${id}`)
        .then(res => {
            dispatch({
                type: GET_USER,
                payload: res.data
            })
        }).catch(err => {
        dispatch({
            type: GET_ERRORS,
            payload: err
        })
    })
}

export const getUser = (id) => dispatch => {
    axios
        .get(`/api/user/show/${id}`)
        .then(res => {
            dispatch({
                type: GET_USER,
                payload: res.data
            })
        }).catch(err => {
        dispatch({
            type: GET_ERRORS,
            payload: err
        })
    })
}

export const removeUser = (id) => dispatch => {
    axios
        .post(`/api/users/remove/${id}`)
        .then(res => {
            dispatch({
                payload: res.data
            })
        }).catch(err => {
           dispatch({
               type: GET_ERRORS,
               payload: err
           })
    })
}
