import React, { Component } from "react";
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import {Container, Row, Col, Card, Button, Form} from "react-bootstrap";
import Basket from "./Basket";


class Order extends Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            previous: []
        };
    }

    render() {
        let userCart = <Basket />
        return (
            <Container className="py-3 px-5">
                <h1 className="h2 text-center mb-5">Validation</h1>
                <Row>
                    <Col>
                        <h5>DÉTAILS DE LA FACTURE</h5>
                        <Form.Row>
                            <Form.Group as={Col} controlId="formGridFirstName">
                                <Form.Label>Prénom</Form.Label>
                                <Form.Control type="text" placeholder="Entrer votre prénom" />
                            </Form.Group>

                            <Form.Group as={Col} controlId="formGridLastName">
                                <Form.Label>Nom</Form.Label>
                                <Form.Control type="text" placeholder="Entrer votre nom" />
                            </Form.Group>
                        </Form.Row>

                        <Form.Group controlId="formGridAddress1">
                            <Form.Label>Email</Form.Label>
                            <Form.Control type="email" placeholder="Entrer votre email" />
                        </Form.Group>

                        <Form.Group controlId="formGridAddress1">
                            <Form.Label>Address</Form.Label>
                            <Form.Control placeholder="1234 Main St" />
                        </Form.Group>

                        <Form.Group controlId="formGridAddress2">
                            <Form.Label>Address 2</Form.Label>
                            <Form.Control placeholder="Apartment, studio, or floor" />
                        </Form.Group>

                        <Form.Row>
                            <Form.Group as={Col} controlId="formGridCity">
                                <Form.Label>City</Form.Label>
                                <Form.Control />
                            </Form.Group>

                            <Form.Group as={Col} controlId="formGridCountry">
                                <Form.Label>Country</Form.Label>
                                <Form.Control />
                            </Form.Group>

                            <Form.Group as={Col} controlId="formGridZip">
                                <Form.Label>Zip</Form.Label>
                                <Form.Control />
                            </Form.Group>
                        </Form.Row>
                    </Col>
                    <Col>
                        <Card>
                            <Card.Header className="text-center">
                                <h5>VOTRE COMMANDE</h5>
                            </Card.Header>
                            <Card.Body>
                                {userCart}
                                <Row className="my-3 px-4">
                                    Expédition:
                                    <div className="ml-auto">
                                        <Form.Check
                                            type="radio"
                                            label="Livraison Standard : 3.20€"
                                            name="livraison"
                                            id="livraisonStandard"
                                            value="3.20"
                                        />
                                        <Form.Check
                                            type="radio"
                                            label="Livraison Express : 27.50€"
                                            name="livraison"
                                            id="livraisonExpress"
                                            value="27.50"
                                        />
                                    </div>
                                </Row>
                                <Card.Text>
                                    Vos données personnelles seront utilisées pour le traitement de votre commande, vous accompagner au cours de votre visite du site web, et pour d’autres raisons décrites dans notre politique de confidentialité.
                                </Card.Text>
                                <Form.Group controlId="formBasicCheckbox">
                                    <Form.Check type="checkbox" label="J’ai lu et j’accepte les conditions générales *" />
                                </Form.Group>
                                <Button className="w-100 px-4" variant="primary" type="submit">
                                    Commander
                                </Button>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Container>
        )
    }
}


Order.propTypes = {
    getPreviousBasket: PropTypes.func.isRequired,
    basket: PropTypes.object.isRequired,
    previous: PropTypes.array.isRequired,
    user: PropTypes.array.isRequired,
};


const mapStateToProps = state => ({
    basket: state.basket.basket,
    previous: state.basket.previous,
    errors: state.errors,
    user: state.auth.user
});


export default connect(mapStateToProps)(Order);
