import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { registerUser } from '../../../actions/authActions';
import { withRouter } from 'react-router-dom';
import { Alert, Button, Card, Col, Form, Row } from 'react-bootstrap';


class Register extends Component {
    constructor() {
        super();
        var today = new Date();
        this.state = {
            email: "",
            password: "",
            firstName: "",
            lastName: "",
            street: "",
            building_name: "",
            city: "",
            country: "",
            zip: "",
            phone: "",
            enterprise: "",
            dateCreated: today,
            active: false,
            invoice: false,
            delivery: false,
            isCommercial: false
        };
        this.toastShow = false;
    }

    onChange = e => {
        switch (e.target.id) {
            case 'active':
            case 'delivery':
            case 'invoice':
            case 'isCommercial':
                this.setState({ [e.target.id]: e.target.checked })
                break;
            default:
                this.setState({ [e.target.id]: e.target.value });
                break;
        }
    };

    onSubmit = e => {
        e.preventDefault();
        this.props.registerUser(this.state, this.toastShow);
    }

    render() {
        const { errors } = this.props;
        return (
            <Row><Col>
                <Card>
                    <Form onSubmit={this.onSubmit} autoComplete="new">
                        <Card.Header as="h5">Ajouter un Utilisateur</Card.Header>
                        <Card.Body>
                            <Form.Row>
                                <Form.Group as={Col} controlId="email">
                                    <Form.Label>Email</Form.Label>
                                    <Form.Control 
                                        onChange={this.onChange}
                                        value={this.state.email}
                                        errors={errors.email}
                                        type="email" 
                                        placeholder="Saisir un e-mail"
                                        autoComplete="new-email"/>
                                </Form.Group>
                                <Form.Group as={Col} controlId="password">
                                    <Form.Label>Mot de Passe</Form.Label>
                                    <Form.Control
                                        onChange={this.onChange}
                                        value={this.state.password}
                                        errors={errors.password}
                                        type="password" 
                                        placeholder="Mot de Passe"
                                        autoComplete="new-password"/>
                                </Form.Group>
                            </Form.Row>
                            {(errors.email) ? <Alert variant="danger">{errors.email.message}</Alert> : null}
                            {(errors.password) ? <Alert variant="danger">{errors.password.message}</Alert> : ""}
                            <Form.Row>
                                <Form.Group as={Col} controlId="lastName">
                                    <Form.Label>Prénom</Form.Label>
                                    <Form.Control
                                        onChange={this.onChange}
                                        value={this.state.lastName}
                                        errors={errors.lastName}
                                        type="text" />
                                </Form.Group>
                                <Form.Group as={Col} controlId="firstName">
                                    <Form.Label>Nom</Form.Label>
                                    <Form.Control
                                        onChange={this.onChange}
                                        value={this.state.firstName}
                                        errors={errors.firstName}
                                        type="text" />
                                </Form.Group>
                            </Form.Row>
                            {(errors.firstName) ? <Alert variant="danger">{errors.firstName.message}</Alert> : ""}
                            {(errors.lastName) ? <Alert variant="danger">{errors.lastName.message}</Alert> : ""}
                            <Form.Row>
                                <Form.Group as={Col} controlId="phone">
                                    <Form.Label>Téléphone</Form.Label>
                                    <Form.Control
                                        onChange={this.onChange}
                                        value={this.state.phone}
                                        errors={errors.phone}
                                        type="tel" />
                                </Form.Group>
                                <Form.Group as={Col} controlId="enterprise">
                                    <Form.Label>Entreprise</Form.Label>
                                    <Form.Control
                                        onChange={this.onChange}
                                        value={this.state.enterprise}
                                        errors={errors.enterprise}
                                        type="text" />
                                </Form.Group>
                            </Form.Row>
                            {(errors.phone) ? <Alert variant="danger">{errors.phone.message}</Alert> : ""}
                            {(errors.enterprise) ? <Alert variant="danger">{errors.enterprise.message}</Alert> : ""}
                        </Card.Body>
                        <Card.Header style={{borderTop: "1px solid rgba(0,0,0,.125)"}}>Adresse</Card.Header>
                        <Card.Body>
                            <Form.Row>
                                <Form.Group as={Col} controlId="street">
                                    <Form.Label>Rue</Form.Label>
                                    <Form.Control
                                        onChange={this.onChange}
                                        value={this.state.street}
                                        errors={errors.street}
                                        type="text" />
                                </Form.Group>
                                {(errors.address) ? <Alert variant="danger">{errors.address.message}</Alert> : ""}
                                <Form.Group as={Col} controlId="building_name">
                                    <Form.Label>Bâtiment</Form.Label>
                                    <Form.Control
                                        onChange={this.onChange}
                                        value={this.state.building_name}
                                        errors={errors.building_name}
                                        type="text" />
                                </Form.Group>
                            </Form.Row>
                            <Form.Row>
                                <Form.Group as={Col} controlId="country">
                                    <Form.Label>Pays</Form.Label>
                                    <Form.Control
                                        onChange={this.onChange}
                                        value={this.state.country}
                                        errors={errors.country}
                                        type="text" />
                                </Form.Group>
                                <Form.Group as={Col} controlId="city">
                                    <Form.Label>City</Form.Label>
                                    <Form.Control
                                        onChange={this.onChange}
                                        value={this.state.city}
                                        errors={errors.city}
                                        type="text" />
                                </Form.Group>
                                <Form.Group as={Col} controlId="zip">
                                    <Form.Label>Zip</Form.Label>
                                    <Form.Control
                                        onChange={this.onChange}
                                        value={this.state.zip}
                                        errors={errors.zip}
                                        type="text"/>
                                </Form.Group>
                            </Form.Row>
                            {(errors.country) ? <Alert variant="danger">{errors.country.message}</Alert> : ""}
                            {(errors.city) ? <Alert variant="danger">{errors.city.message}</Alert> : ""}
                            <Form.Row>
                                <Form.Group as={Col} controlId="isCommercial">
                                    <Form.Check
                                        custom
                                        onChange={this.onChange}
                                        checked={this.state.isCommercial}
                                        type="checkbox"
                                        errors={errors.isCommercial}
                                        label="Commercial ?"/>
                                </Form.Group>
                                <Form.Group as={Col} controlId="active">
                                    <Form.Check
                                        custom
                                        onChange={this.onChange}
                                        checked={this.state.active}
                                        type="checkbox"
                                        errors={errors.active}
                                        label="Active ?"/>
                                </Form.Group>
                                <Form.Group as={Col} controlId="invoice">
                                    <Form.Check
                                        custom
                                        onChange={this.onChange}
                                        checked={this.state.invoice}
                                        type="checkbox"
                                        errors={errors.invoice}
                                        label="Facturation ?"/>
                                </Form.Group>
                                <Form.Group as={Col} controlId="delivery">
                                    <Form.Check
                                        custom
                                        onChange={this.onChange}
                                        checked={this.state.delivery}
                                        type="checkbox"
                                        errors={errors.delivery}
                                        label="Livraison ?"/>
                                </Form.Group>
                            </Form.Row>
                            {(errors.isCommercial) ? <Alert variant="danger">{errors.isCommercial.message}</Alert> : ""}
                            <Button variant="primary" type="submit">
                                Inscrire
                            </Button>
                        </Card.Body>
                    </Form>
                </Card>
            </Col></Row>
        )
    }
}


Register.propTypes = {
    registerUser: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
};


const mapStateToProps = state => ({
    auth: state.auth,
    errors: state.errors
});


export default withRouter(connect(mapStateToProps, { registerUser })(Register));
