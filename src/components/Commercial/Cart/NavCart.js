import React, { Component } from "react";
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { NavDropdown } from "react-bootstrap";
import { FaShoppingBasket } from "react-icons/fa";
import { Link } from "react-router-dom"


class NavCart extends Component {

    render() {
        let userCart = <NavDropdown.Item key="empty">Votre panier est vide</NavDropdown.Item>

        if(this.props.basket) {
            if (this.props.basket.items.length) {
                userCart = this.props.basket.items.map((product) => {
                    return (
                        <NavDropdown.Item key={product.id}>{product.name} x{product.quantity}</NavDropdown.Item>
                    )
                });
                userCart.push(<NavDropdown.Item key="total">Total
                    : {parseFloat(this.props.basket.total_price.toFixed(2))}€</NavDropdown.Item>)
            }
        }

        return (
            <NavDropdown title={<FaShoppingBasket/>} id="basic-nav-dropdown" className="mx-2">
                {userCart}
                <NavDropdown.Divider />
                <div className="text-center">
                    <Link className="btn btn-primary" to="/commercial/cart">
                        Voir panier
                    </Link>
                    <Link className="btn btn-primary w-80 mt-2" to="/commercial/order">
                        Passer commande
                    </Link>
                </div>
            </NavDropdown>
        )
    }
}


NavCart.propTypes = {
    basket: PropTypes.object.isRequired
};


const mapStateToProps = state => ({
    basket: state.basket.basket,
    errors: state.errors
});


export default connect(mapStateToProps)(NavCart);
