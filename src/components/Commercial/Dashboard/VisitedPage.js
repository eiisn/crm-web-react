import React, { Component } from 'react';
import { Container } from 'react-bootstrap';


class VisitedPage extends Component{
    render() {
        return (
            <Container fluid className="px-4 py-3">
                <p>Visited Pages</p>
            </Container>
        )
    }
}

export default VisitedPage;
