import {
    GET_PRODUCT
} from "../actions/types";

const initialState = {
    items: []
};

export default function (state = initialState, action) {
    switch (action.type) {
        case GET_PRODUCT:
            return {
                ...state,
                items: action.payload.data
            }
        default:
            return state;
    }
}
