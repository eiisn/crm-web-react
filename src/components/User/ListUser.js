import React, { Component } from 'react';
import {Alert, Button, ButtonGroup, Container, ListGroup} from 'react-bootstrap';
import {FaEye, FaPen, FaPlus, FaTrashAlt, FaUsers} from "react-icons/fa";
import {connect} from "react-redux";
import PropTypes from "prop-types";
import { getUsers, removeUser } from './userActions';
import {Link, Route, Switch} from "react-router-dom";
import AddUser from "./addUser";
import ProfileUser from "./ProfileUser";
import moment from "moment";

class ListUser extends Component {
    componentDidMount() {
        this.props.getUsers();
    }

    render() {
        const content = this.props.users.map(user => (
            <ListGroup.Item className="d-flex align-items-center">
                <div className="mr-5">{user.firstName} {user.lastName}</div>
                <div className="mx-5">{user.email}</div>
                <div className="mx-5">
                    {moment(user.dateCreated).format('DD-MM-YYYY')}
                </div>
                <ButtonGroup className="ml-auto">
                    <Button className="btn-success"><FaPen /></Button>
                    <Button as={Link} to="/commercial/user/profile/"><FaEye /></Button>
                    <Button onClick={() => this.props.removeUser(user._id)} as={Link} to="/commercial/users/" className="btn-danger"><FaTrashAlt /></Button>
                </ButtonGroup>
            </ListGroup.Item>
        ));

        return (
            <Container className="py-3 px-5">
                <Switch>
                    <Route path="/commercial/users/add">
                        <AddUser />
                    </Route>
                    <Route path="/commercial/user/profile/">
                        <ProfileUser />
                    </Route>
                    <Route path="/commercial/users">
                        <h2><FaUsers/> Liste des utilisateurs</h2>
                        <Alert variant="dark" className="text-right">
                            <Button as={Link} to="/commercial/users/add" className="btn-success"><FaPlus /></Button>
                        </Alert>
                        <ListGroup variant="flush">
                            { content }
                        </ListGroup>
                    </Route>
                </Switch>
            </Container>
        )
    };
}

ListUser.propTypes = {
    users: PropTypes.array.isRequired,
    errors: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
    users: state.users.items,
    errors: state.errors
})

export default connect(mapStateToProps, { getUsers, removeUser })(ListUser)
