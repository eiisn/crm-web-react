import React, { Component } from 'react';
import './App.css';

import jwt_decode from "jwt-decode";
import store from "./store";

import setAuthToken from "./utils/setAuthToken";
import { setCurrentUser, logoutUser } from "./actions/authActions";
import { setAddedProducts } from "./actions/productActions";

import { Switch, Route } from 'react-router-dom';

import Client from './components/Client/Client';
import Login from './components/User/Auth/Login';
import Commercial from './components/Commercial/Commercial';


// Check for token to keep user logged in
if (localStorage.token) {
    // Set auth token header auth
    const token = localStorage.token;
    setAuthToken(token);
    // Decode token and get user info and exp
    const decoded = jwt_decode(token);
    // Set user and isAuthenticated
    store.dispatch(setCurrentUser(decoded));
    store.dispatch(setAddedProducts());
    // Check for expired token
    const currentTime = Date.now() / 1000; // to get in milliseconds
    if (decoded.exp < currentTime) {
        // Logout user
        store.dispatch(logoutUser());
        // Redirect to login
        window.location.href = "/login";
    }
}


class App extends Component {
    
    constructor(props) {
        super(props)
        this.state = {
            isAuthenticated: false,
            isCommercial: false,
            user: {}
        }
    }

    componentDidMount() {
        // console.log("Authenticated : " + this.props.isAuthenticated)
        // console.log("Commercial    : " + this.props.isCommercial)
        this.setState({
            isAuthenticated: this.props.isAuthenticated,
            isCommercial: this.props.isCommercial,
            user: this.props.user
        })
    }

    render() {
        return (
            <Switch>
                <Route path="/commercial">
                    <Commercial user={ this.state.user } />
                </Route>
                <Route path="/client">
                    <Client />
                </Route>
                <Route path="/">
                    <Login />
                </Route>
            </Switch>
        )
    }
}


export default App;
