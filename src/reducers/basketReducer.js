import {
    GET_BASKET,
    GET_PREVIOUS_BASKET,
    ADD_TO_BASKET,
    REMOVE_FROM_BASKET
} from '../actions/types'


const initialState = {
    basket: {
        _id: "-1",
        items: [],
        total_price: 0
    },
    previous: []
}

export default function(state = initialState, action) {
    switch (action.type) {
        case GET_BASKET:
            return {
                ...state,
                basket: action.payload
            }
        case GET_PREVIOUS_BASKET:
            return {
                ...state,
                previous: action.payload
            }
        case ADD_TO_BASKET:
            return {
                ...state,
                basket: action.payload
            }
        case REMOVE_FROM_BASKET:
            return {
                ...state,
                basket: action.payload
            }
        default:
            return state
    }
}