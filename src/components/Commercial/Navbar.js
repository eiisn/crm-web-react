import React, { Component } from 'react';
import {Navbar as BNavbar, Nav, NavDropdown} from 'react-bootstrap';
import {FaSignOutAlt, FaUserAlt, FaUserFriends, FaCog, FaReceipt, FaVial} from 'react-icons/fa';
import {Link, withRouter} from "react-router-dom";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {logoutUser} from "../../actions/authActions";
import NavCart from "./Cart/NavCart";


class Navbar extends Component {
    onLogoutClick = e => {
        e.preventDefault();
        this.props.logoutUser();
        window.location.reload();
    };

    render() {
        return (
            <BNavbar bg="dark" variant="dark">
                <BNavbar.Brand href="#home"><FaVial /> Buena Vista</BNavbar.Brand>
                <BNavbar.Toggle aria-controls="basic-navbar-nav" />
                <BNavbar.Collapse id="basic-navbar-nav">
                    <Nav className="ml-auto mr-3">
                        <NavCart />
                        <NavDropdown className="mx-2" title={<FaUserAlt />} id="basic-nav-dropdown">
                            <NavDropdown.Item as={Link} to="/commercial/profile/"><FaUserFriends /> Profile</NavDropdown.Item>
                            <NavDropdown.Item href="#action/3.2"><FaReceipt /> My shipments</NavDropdown.Item>
                            <NavDropdown.Item as={Link} to="/commercial/settings"><FaCog /> Settings</NavDropdown.Item>
                            <NavDropdown.Divider />
                            <NavDropdown.Item onClick={this.onLogoutClick} ><FaSignOutAlt /> Logout</NavDropdown.Item>
                        </NavDropdown>
                    </Nav>
                </BNavbar.Collapse>
            </BNavbar>
        )
    }
}

Navbar.propTypes = {
    logoutUser: PropTypes.func.isRequired,
    isAuthenticated: PropTypes.bool.isRequired,
};


const mapStateToProps = state => ({
    isAuthenticated: state.auth.isAuthenticated,
});


export default connect(mapStateToProps, { logoutUser })(withRouter(Navbar));
