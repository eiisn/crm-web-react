import React, { Component } from 'react';
import { Alert, Button, Card, Col, Container, Form, Row } from 'react-bootstrap';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { loginUser } from '../../../actions/authActions'
import { withRouter } from 'react-router-dom';


class Login extends Component {

    constructor() {
        super();
        this.state = {
            email: "",
            password: "",
        };
    }

    componentDidMount() {
        if (this.props.auth.isAuthenticated) {
            if (this.props.auth.isCommercial) {
                this.props.history.push('/commercial')
            } else {
                this.props.history.push('/client')
            }
        }
    }

    onChange = e => {
        this.setState({ [e.target.id]: e.target.value })
    }

    onSubmit = e => {
        e.preventDefault();
        const userData = {
            email: this.state.email,
            password: this.state.password
        }
        this.props.loginUser(userData)
    }

    render() {
        const { errors } = this.props;
        return (
            <Container className="my-5 w-50">
                <div className="text-center">
                    <h1>Connexion</h1>
                    <Row>
                        <Col>
                            <Card className="text-left">
                                <Card.Body>
                                    <Form onSubmit={this.onSubmit}>
                                        <Form.Group controlId="email">
                                            <Form.Label>Adresse Email</Form.Label>
                                            <Form.Control
                                                onChange={this.onChange}
                                                value={this.state.email}
                                                errors={errors.email}
                                                type="email"
                                                placeholder="Enter email"
                                                isInvalid={errors.email} />
                                            {(errors.email) ? <Alert variant="danger">{errors.email}</Alert> : ""}
                                            <Form.Text className="text-muted">
                                                Ne partager pas vos identifiants avec quelqu'un d'autre.
                                            </Form.Text>
                                        </Form.Group>
                                        <Form.Group controlId="password">
                                            <Form.Label>Mot de Passe</Form.Label>
                                            <Form.Control
                                                onChange={this.onChange}
                                                value={this.state.password}
                                                errors={errors.password}
                                                type="password"
                                                placeholder="Password"
                                                isInvalid={errors.password} />
                                            {(errors.password) ? <Alert variant="danger">{errors.password}</Alert> : ""}
                                        </Form.Group>
                                        <div className="w-100 text-center">
                                            <Button variant="primary" type="submit">
                                                Connexion
                                            </Button>
                                        </div>
                                    </Form>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                </div>
            </Container>
        )
    }
}


Login.propTypes = {
    loginUser: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
    email: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    user: state.auth.user,
    email: state,
    auth: state.auth,
    errors: state.errors
});


export default withRouter(connect(mapStateToProps, {loginUser})(Login));
