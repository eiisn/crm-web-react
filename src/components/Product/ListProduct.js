import React, { Component } from 'react';
import PropTypes from "prop-types";
import { connect } from 'react-redux'
import { Link } from 'react-router-dom';
import { Container, Alert, Row } from "react-bootstrap";
import { getProduct } from '../../actions/productActions'
import ProductItem from './ProductItem';
import {FaCapsules} from "react-icons/fa";


class ListProduct extends Component{

    componentDidMount() {
        this.props.getProduct();
    }

    render() {
        let error = ""
        if ("message" in this.props.errors) {
            error = this.props.errors.message
        }

        const content = this.props.products.map(product => (
            <ProductItem key={product.id} id={product.id} name={product.name} list_price={product.list_price}/>
        ));

        return (
            <Container fluid>
                <div className="py-3 px-5">
                    <h2><FaCapsules /> Liste des produits</h2>
                    {error ? <Alert variant="danger">{error} <Link to="/">Retour</Link>
                    </Alert> : ""}
                    <Row>
                        {content}
                    </Row>
                </div>
            </Container>
        )
    }
}

ListProduct.propTypes = {
    products: PropTypes.array.isRequired,
    errors: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
    products: state.products.items,
    errors: state.errors
})

export default connect(mapStateToProps, { getProduct })(ListProduct)
