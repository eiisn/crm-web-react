import {GET_ERRORS} from "./types";
import axios from "axios";

export const setShipment = (cartData, history) => dispatch => {
    axios
        .post("/api/set-shipment", {
            "currency": "EUR",
            "information": cartData.date,
            "customer": 1,
            "detail": cartData.products
        })
        .then(res => {
            history.push("/products")
        }) // re-direct to login on successful register
        .catch(err => {
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data.errors
            })
        })
};
