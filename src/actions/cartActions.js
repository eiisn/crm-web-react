import {
    ADD_TO_CART,
    REMOVE_ITEM,
    SUB_QUANTITY,
    ADD_QUANTITY,
    SET_ADDED_PRODUCT,
    GET_ERRORS,
    GET_CARTS,
    GET_CART
} from "./types";
import axios from "axios";


//add cart action
export const addToCart = (id) => {
    return {
        type: ADD_TO_CART,
        id
    }
}

//remove item action
export const removeItem = (id) => {
    return {
        type: REMOVE_ITEM,
        id
    }
}

//subtract qt action
export const subtractQuantity = (id) => {
    return {
        type: SUB_QUANTITY,
        id
    }
}

//add qt action
export const addQuantity = (id) => {
    return {
        type: ADD_QUANTITY,
        id
    }
}

export const setAddedProducts = decoded => {
    return {
        type: SET_ADDED_PRODUCT,
        payload: decoded
    };
};

export const addCart = (cartData, history) => dispatch => {
    axios
        .post("/api/add-cart", cartData)
        .then(res => history.push("/product")) // re-direct to login on successful register
        .catch(err =>
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data.message
            })
        );
};

export const getCarts = () => dispatch => {
    axios
        .get("http://localhost:3030/api/get-carts")
        .then(carts => {
            dispatch({
                type: GET_CARTS,
                payload: carts.data
            })
        }).catch(err => {
        dispatch({
            type: GET_ERRORS,
            payload: err
        })
    })
};

export const getCart = (idUser) => dispatch => {
    axios
        .get("http://localhost:3030/api/get-cart",{
            params: {
                ID: idUser
            }
        })
        .then(cart => {
            dispatch({
                type: GET_CART,
                payload: cart.data.products
            })
        })
        .catch(err => {
            dispatch({
                type: GET_ERRORS,
                paylaod: err
            })
        })
}