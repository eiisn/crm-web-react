import React, { Component } from 'react';
import {Nav, Col} from 'react-bootstrap';
import {FaTachometerAlt, FaUsers, FaBoxOpen, FaCapsules} from 'react-icons/fa';
import { Link } from 'react-router-dom';


class Sidebar extends Component {
    render() {
        return (
            <Col className="sidebar bg-bv-primary" lg="2">
                <Nav defaultActiveKey="/home" className="flex-column">
                    <Nav.Link action as={Link} to="/"><FaTachometerAlt /> Dashboard</Nav.Link>
                    <Nav.Link action as={Link} to="/commercial/list-product"><FaCapsules /> Produits</Nav.Link>
                    <Nav.Link action as={Link} to="/commercial/shipments"><FaBoxOpen/> Commandes</Nav.Link>
                    <Nav.Link action as={Link} to="/commercial/users"><FaUsers/> Utilisateurs</Nav.Link>
                </Nav>
            </Col>
        )
    }
}

export default Sidebar;
