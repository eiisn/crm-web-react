import axios from "axios";
import {
    GET_PRODUCT,
    GET_ERRORS,
    SET_ADDED_PRODUCT
} from './types';


// Get product from CRM API NodeJS
export const getProduct = () => dispatch => {
    axios
        .get("/api/products")
        .then(res => {
            dispatch({
                type: GET_PRODUCT,
                payload: res.data
            })
        }).catch(err => {
            dispatch({
                type: GET_ERRORS,
                payload: err
            })
        })
}

// Set logged in user
export const setAddedProducts = decoded => {
    return {
        type: SET_ADDED_PRODUCT,
        payload: decoded
    };
};