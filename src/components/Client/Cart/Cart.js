import React, { Component } from "react";
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import {Row, Col, Card, Accordion, Button, Table, Form} from "react-bootstrap";
import { Link } from "react-router-dom"
import Basket from "./Basket";
import { getPreviousBasket } from "../../../actions/basketActions";
import {FaShoppingBasket} from "react-icons/fa";


class NavCart extends Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            previous: []
        };
    }

    componentDidMount() {
        this.props.getPreviousBasket();
    }

    render() {
        let previousCart = <p>Aucune commande</p>

        if (this.props.previous.length) {
            previousCart = this.props.previous.map((basket, idx) => (
                <Card>
                    <Card.Header>
                        <Accordion.Toggle as={Button} variant="link" eventKey={idx}>
                            Commande N°{idx}
                        </Accordion.Toggle>
                    </Card.Header>
                    <Accordion.Collapse eventKey={idx}>
                        <Card.Body>
                            <Basket items={basket.items} total_cost={basket.total_cost} />
                        </Card.Body>
                    </Accordion.Collapse>
                </Card>
            ))
        }

        let totalShipment = this.props.basket.total_price + 3.20
        return (
            <div className="py-3 px-5">
                <h1 className="h2 mb-5"><FaShoppingBasket /> Votre Panier</h1>
                <Row>
                    <Table responsive hover bordered>
                        <thead>
                        <tr className="bg-dark text-light">
                            <th style={{width: "60%"}}>Information produit</th>
                            <th style={{width: "10%"}} className="text-center">Prix</th>
                            <th style={{width: "20%"}} className="text-center">Quantité</th>
                            <th style={{width: "10%"}} className="text-center">Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        <Basket />
                        </tbody>
                    </Table>
                </Row>
                <Row>
                    <Col>
                        <Card>
                            <Card.Header as="h4">Total Panier</Card.Header>
                            <Card.Body>
                                <Row className="my-3 px-4">
                                    Sous-total
                                    <h6 className="ml-auto">{parseFloat(this.props.basket.total_price.toFixed(2))}€</h6>
                                </Row>
                                <hr/>
                                <Row className="my-3 px-4">
                                    Expédition:
                                    <div className="ml-auto">
                                        <Form.Check
                                            type="radio"
                                            label="Livraison Standard : 3.20€"
                                            name="livraison"
                                            id="livraisonStandard"
                                            value="3.20"
                                        />
                                        <Form.Check
                                            type="radio"
                                            label="Livraison Express : 27.50€"
                                            name="livraison"
                                            id="livraisonExpress"
                                            value="27.50"
                                        />
                                    </div>
                                </Row>
                                <hr/>
                                <Row className="my-4 px-4">
                                    Total
                                    <h4 className="ml-auto">
                                        {parseFloat(totalShipment.toFixed(2))}€
                                    </h4>
                                </Row>
                                <hr/>
                                <Link className="btn btn-primary text-uppercase h3 w-100" to="/client/order">
                                    Passer commande
                                </Link>
                                <Link className="btn btn-secondary text-uppercase h3 w-100" to="/client">
                                    Continuer vos achats ->
                                </Link>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
                <hr/>
                <Row>
                    <Col>
                        <Card>
                            <Card.Header as="h4">Mes commandes précédentes</Card.Header>
                            <Card.Body>
                                <Accordion>
                                    {previousCart}
                                </Accordion>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    }
}

NavCart.propTypes = {
    getPreviousBasket: PropTypes.func.isRequired,
    basket: PropTypes.object.isRequired,
    previous: PropTypes.array.isRequired,
};

const mapStateToProps = state => ({
    basket: state.basket.basket,
    previous: state.basket.previous,
    errors: state.errors
});

export default connect(mapStateToProps, { getPreviousBasket })(NavCart);
