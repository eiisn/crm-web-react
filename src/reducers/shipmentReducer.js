import {

    SET_SHIPMENT
} from "../actions/types";

const initialState = {
    items: []
};

export default function (state = initialState, action) {
    switch (action.type) {
        case SET_SHIPMENT:
            return {
                ...state,
                items: action.payload
            }
        default:
            return state;
    }
}
