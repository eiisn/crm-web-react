import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {Button, ButtonGroup, Card, Col} from 'react-bootstrap';
import { connect } from 'react-redux';
import { addToBasket } from '../../actions/basketActions';
import {FaCartPlus, FaEye} from "react-icons/fa";

const pillPic = [
    "https://cdn.shopify.com/s/files/1/0019/1843/5381/products/Box_Mockups_OK_3_241036b2-175b-484e-960d-3c632a6e0f48_800x.jpg",
    "https://www.decolore.net/wp-content/uploads/2018/08/pills-box-packaging-psd-mockup-templates-cover.png",
    "https://cdn.shopify.com/s/files/1/0019/1843/5381/products/Box_Mockups_OK_2_b6ed3ef7-bf46-4127-b18f-57f96fffe450_800x.jpg",
    "https://image.freepik.com/free-psd/medical-pillbox-mock-up_110893-1582.jpg",
    "https://image.freepik.com/free-psd/pill-bottle-mockup_77847-103.jpg",
    "https://s3.envato.com/files/245086193/07_PillsBox-Mockup2-4.jpg",
    "https://i.pinimg.com/originals/44/4d/c1/444dc1f68ad244809e62af43420b6b5e.jpg"
]

class Product extends Component {

    constructor(props) {
        super(props);
        this.state = {
            id: props.id,
            name: props.name,
            list_price: props.list_price,
            basketId: null
        }
    }

    onClick = () => {
        console.log("add to basket :");
        console.log(this.props);
        console.log(this.state);
        console.log("--------------");
        this.props.addToBasket({
            basketId: this.props.basketId,
            product: {
                id: this.state.id,
                name: this.state.name,
                list_price: this.state.list_price
            }
        })
    }

    render() {
        return(
            <Col md="3" className="my-3">
                <Card key={this.state.id}>
                    <Card.Img variant="top" src={pillPic[Math.floor(Math.random() * (pillPic.length - 1) + 1) - 1]} />
                    <Card.Body>
                        <Card.Title>{this.state.name}</Card.Title>
                        <span><b>Price: {this.state.list_price}$</b></span>
                        <ButtonGroup className="float-right">
                            <Button><FaEye /></Button>
                            <Button onClick={() => { this.onClick() }} className="btn-success"><FaCartPlus /></Button>
                        </ButtonGroup>
                    </Card.Body>
                </Card>
            </Col>
        )
    }
}


Product.propTypes = {
    addToBasket: PropTypes.func.isRequired,
    basketId: PropTypes.string.isRequired
}

const mapStateToProps = state => ({
    basketId: state.basket.basket._id
})


export default connect(mapStateToProps, { addToBasket })(Product);
